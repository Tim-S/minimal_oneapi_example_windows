# Minimal OneAPI Example (Windows) 

## Prerequisites

* [Intel OneApi (incl oneMkl)](https://software.intel.com/content/www/us/en/develop/tools/oneapi/base-toolkit/download.html) 
* [Visual Studio 2019 (Build Tools are sufficient)](https://visualstudio.microsoft.com/de/downloads/)

## Build

Open "Intel oneAPI command prompt for Intel 64 for Visual Studio 2019"

```
mkdir build 
cd build
```

```
cmake -T "Intel(R) oneAPI DPC++ Compiler" -DCMAKE_CXX_COMPILER=dpcpp ..
```

```
cmake --build .
```

```
./Debug/app.exe
```