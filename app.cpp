#include <CL/sycl.hpp>

using namespace sycl;

static const int COUNT = 5;

int main(){
  queue queue;

  std::cout << 
    "Device: " << queue.get_device().get_info<info::device::name>() << std::endl;
  
  int *data = malloc_shared<int>(COUNT, queue);
  for(int i=0; i<COUNT; i++) 
    data[i] = i;

  queue.parallel_for(range<1>(COUNT), [=] (id<1> i){
    data[i] *= 7;
  }).wait();

  for(int i=0; i<COUNT; i++) 
    std::cout << data[i] << std::endl;
  free(data, queue);
  return 0;
}